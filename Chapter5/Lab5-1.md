# 課題5.1: TLS アクセスの設定

**概要**

kubectl はKubernetes のAPI 呼び出しを行うコマンドラインツールです。適切なTLS キーを利用すると、golang クライアントやcurl でのAPI 呼び出しが可能になります。kube-apiserver へのAPI 呼び出しにより、PodSpec などの対象状態の取得や設定ができます。リクエストが新しい状態を示していれば、Kubernetes Control Plane は、オブジェクトの現在の状態と設定した状態が一致するまでクラスタを更新します。最終的な状態によっては、複数のリクエストが必要となる可能性があります。例えば、ReplicaSet を削除する場合、まずレプリカ数を0 に設定してから、ReplicaSet を削除します。  
API リクエストはJSON 形式で情報を渡す必要があります。kubectl はAPI リクエストを行う際に、.yamlファイルをJSON に変換します。API リクエストには設定が多くありますが、Pod(コンテナ) をデプロイする際にapiVersion、kind、metadata、specの設定が必須です。spec のフィールドは作成するオブジェクトに依存します。  
まずは、kube-apiserver へのリモートアクセスを設定してから、API の他の部分をさらに見ていきます。  

**1. まずホームディレクトリに戻りkubectl 設定ファイルを確認します。3 つの証明書とAPI サーバアドレスを利用します。**
```
student@setX-master:~$ cd ; pwd
student@setX-master:~$ less $HOME/.kube/config

< 省略>
```

**2. 証明書情報を利用して変数を作成します。環境変数を設定後、設定した値を確認しましょう。まず、client-certificate-data キーを設定します。**
```
student@setX-master:~$ export client=$(grep client-cert $HOME/.kube/config |cut -d" " -f 6)
student@setX-master:~$ echo $client

LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM4akNDQWRxZ0F3SUJ  
BZ0lJRy9wbC9rWEpNdmd3RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0  
ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB4TnpFeU1UTXhOelEyTXpKY  
UZ3MHhPREV5TVRNeE56UTJNelJhTURReApGekFWQmdOVkJBb1REbk41YzNS  
< 省略>
```

**3. ほぼ同じコマンドですが、今回はclient-key-data をkey 変数に設定します。**
```
student@setX-master:~$ export key=$(grep client-key-data $HOME/.kube/config |cut -d " " -f 6)
student@setX-master:~$ echo $key

< 省略>
```

**4. 最後に、certificate-authority-data をauth 変数に設定します。**
```
student@setX-master:~$ export auth=$(grep certificate-authority-data $HOME/.kube/config |cut -d " " -f 6)
student@setX-master:~$ echo $auth

< 省略>
```

**5. 次にcurl で利用できるように、環境変数に設定した値をエンコードしファイルに保存します。**
```
student@setX-master:~$ echo $client | base64 -d - > ./client.pem
student@setX-master:~$ echo $key | base64 -d - > ./client-key.pem
student@setX-master:~$ echo $auth | base64 -d - > ./ca.pem
```

**6. 設定ファイルからAPI サーバのURL を確認します。ホスト名やIP アドレスが異なる場合があります。**
```
student@setX-master:~$ kubectl config view |grep server

server: https://k8scp:6443
```

**7. curl コマンドと保存したファイルを利用して、API サーバに接続します。先のコマンドで出力されたホスト名やIP アドレスを使います。**
```
student@setX-master:~$ curl --cert ./client.pem --key ./client-key.pem --cacert ./ca.pem https://k8scp:6443/api/v1/pods

 {
  "kind": "PodList",
  "apiVersion": "v1",
  "metadata": {
    "selfLink": "/api/v1/pods",
    "resourceVersion": "239414"
  },
< 省略>
```

**8. コマンドが成功したら、新しいPod を作成するためにJSON ファイルを作成します。find を使用して、tarball の出力でこのファイルを検索することを忘れないでください。なお、このファイルはLFS458-JP 内に含まれています。**
```
student@setX-master:~$ vim curlpod.json
```

![chap5-2](/uploads/6b28416942620d0b29b97f4b53c3ba30/chap5-2.png)

[curlpod.json](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter5/labfiles/curlpod.json)


**9. 保存したJSON ファイルをXPOST してAPI 呼び出しを行うcurl コマンドを実行します。利用するスケジューラやTaint など、多くの内容を出力されます。出力内容を確認してみましょう。Pod が作成中なので、最後の数行はPending 状態になっているはずです。**
```
student@setX-master:~$ curl --cert ./client.pem --key ./client-key.pem --cacert ./ca.pem https://k8scp:6443/api/v1/namespaces/default/pods -XPOST -H'Content-Type: application/json' -d@curlpod.json

{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "curlpod",
< 省略>
```

**10. 新しいPod が作成され、ステータスがRunning になっていることを確認します。**
```
student@setX-master:~$ kubectl get pods

NAME      READY   STATUS    RESTARTS   AGE
curlpod   1/1     Running   0          45s
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter5/Lab5-2.md)
