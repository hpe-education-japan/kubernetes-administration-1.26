# 課題3.4: シンプルなアプリケーションをデプロイ

シンプルなアプリケーションをデプロイできるかテストします。今回はnginx ウェブサーバです。  

**1. コンテナ内のアプリケーションをデプロイするKubernetes オブジェクトである、Deployment を新しく作成します。コンテナが実行され、あるべき数と実際の数が一致していることを確認します。**
```
student@setX-master:~$ kubectl create deployment nginx --image=nginx

deployment.apps/nginx created
```
```
student@setX-master:~$ kubectl get deployments

NAME  READY UP-TO-DATE AVAILABLE AGE
nginx 1/1   1          1         8s
```

**2. Deployment の詳細を確認します。サブコマンドやリソースは自動補完を利用できることをお忘れなく。**
```
student@setX-master:~$ kubectl describe deployment nginx

Name:                  nginx
Namespace:             default
CreationTimestamp:     Mon, 23 Apr 2019 22:38:32 +0000
Labels:                app=nginx
Annotations:           deployment.kubernetes.io/revision: 1
Selector:              app=nginx
Replicas:              1 desired | 1 updated | 1 total | 1 ava....
StrategyType:          RollingUpdate
MinReadySeconds:       0
RollingUpdateStrategy: 25% max unavailable, 25% max surge
< 省略>
```

**3. 新しいアプリケーションをプル・デプロイするためにクラスタが行った基本的な手順を確認します。出力が数行表示されているはずです。各メッセージの最初のカラムに作成時からの経過時間が表示されています。JSON では順番が保持されないため、LAST SEEN に表示される時刻は時刻順通りには表示されません。時間が経つと古いメッセージから削除されます。**
```
student@setX-master:~$ kubectl get events

< 省略>
```

**4. 出力内容はyaml フォーマットでも表示できるので、再度同じ、あるいは新しいDeployment を作成するために利用できます。出力内容をyaml に変更してみましょう。現在デプロイされているDeployment のステータス情報は後半に出力されます。**
```
student@setX-master:~$ kubectl get deployment nginx -o yaml
```

![chap3-3](/uploads/f83b5e4c2f85fc1c229d39a0653028b3/chap3-3.png)

**5. 再度コマンドを実行し、出力内容をファイルにリダイレクトします。次にファイルを編集します。creationTimestamp とresourceVersionとuid の行を削除します。そして、上記の行が既に削除済みなら120 行あたりにあるはずのstatus: 以降の行を削除します。(テキストにはSelfLinkを削除と記載されていますが実際は存在しません)**
```
student@setX-master:~$ kubectl get deployment nginx -o yaml > first.yaml
student@setX-master:~$ vim first.yaml
< 先で説明した行を削除します>
```

**6. 既存のDeployment を削除します。**
```
student@setX-master:~$ kubectl delete deployment nginx

deployment.apps "nginx" deleted
```

**7. 今度はファイルを利用して、再度Deployment を作成します。**
```
student@setX-master:~$ kubectl create -f first.yaml

deployment.apps/nginx created
```

**8. 今回繰り返した手順の出力内容と、最初の出力内容を比較します。削除したcreationTimestamp、resourceVersion、uidが新しいファイルに存在するはずです。これらはリソースを作成するたびに作成されるので、衝突や間違った情報を防ぐためにyaml ファイルから削除しておく必要があるかもしれません。タイムスタンプが違っていることもあるかもしれません。status: もハードコードすべきではありません。**  
```
student@setX-master:~$ kubectl get deployment nginx -o yaml > second.yaml
student@setX-master:~$ diff first.yaml second.yaml

< 省略>
```

**9. 生の出力を使って作業したので、今度は便利なYAML やJSON を作成する別の2 つの方法を見てみましょう。--dry-run オプションを使用して、オブジェクトを作成せずに内容を確認します。先のnginx Deployment のみが見つかるはずです。この出力には、以前削除したオブジェクト固有の情報は含んでいませんが、さまざまな出力を含みます。**
```
student@setX-master:~$ kubectl create deployment two --image=nginx --dry-run=client -o yaml
```
![chap3-4](/uploads/4731a47e199b5ee82164ea81454f40d7/chap3-4.png)

```
student@setX-master:~$ kubectl get deployment

NAME  READY UP-TO-DATE AVAILABLE AGE
nginx 1/1   1          1         7m
```

**10. 既存のオブジェクトは、すぐに利用可能なYAML 出力として表示できます。既存のnginx Deployment を見てみましょう。**
```
student@setX-master:~$ kubectl get deployments nginx -o yaml
```
![chap3-5](/uploads/4f79fc47f61be668dfd314cfdd633836/chap3-5.png)

**11. JSON 出力としても表示することができます。**
```
student@setX-master:~$ kubectl get deployment nginx -o json
```
![chap3-11](/uploads/de4bc2d5bd8e08b84df29a5d68eaeef6/chap3-11.png)

**12. 新たにデプロイしたnginx コンテナは軽量なウェブサーバです。このウェブサーバのデフォルトのホーム画面を見るためにService を作成する必要があります。まずコマンドのヘルプを確認しましょう。出力の前半に実行例が表示されます。**
```
student@setX-master:~$ kubectl expose -h
```
< 省略>  

**13. 次に、ウェブサーバへのアクセス設定を行いましょう。利用するポートを指定していないため、エラーとなるはずです。**
```
student@setX-master:~$ kubectl expose deployment/nginx

error: couldn't find port via --port flag or introspection  
See 'kubectl expose -h' for help and examples. 
```

**14. オブジェクトの設定を無停止で変更するには、apply とedit とpatch のサブコマンドを利用します。apply コマンドは今回設定したい値、現在の設定、1 つ前の過去の設定の3 つを比較し、加えるべき変更を決定します。記述していないフィールドは影響を受けません。edit 機能はget を実行し、エディタを開き、apply を実行します。JSON パッチとマージパッチの組み合わせ、あるいは戦略的なマージパッチ機能で、その場でAPI オブジェクトを更新することができます。  
設定内に初期化後の更新が不可能なリソースフィールドがある場合、replace --force オプションで停止して更新を行う必要があります。これはリソースを一旦削除してから再作成します。  
ファイルを修正します。31行目あたりにコンテナ名があるので、次のようにポート情報を追加します。**
```
student@setX-master:~$ vim first.yaml
```
![chap3-7](/uploads/dc477790ecf05bdef0f12689d1f96a89/chap3-7.png)

[first.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter3/labfiles/first.yaml)

**15. オブジェクトをどのように作成したかによって、一旦Deployment を終了して新しく作成するreplace を使用しなければいけない場合があります。**
```
student@setX-master:~$ kubectl replace -f first.yaml

deployment.apps/nginx replaced
```

**16. Pod とDeployment を確認します。両者のAGE を確認し、Pod が再作成されたことを確認しましょう。**
```
student@setX-master:~$ kubectl get deploy,pod

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx   1/1     1            1           9m15s

NAME                        READY   STATUS    RESTARTS   AGE
pod/nginx-d46f5678b-x9872   1/1     Running   0          32s
```

**17. リソースを再度公開します。今回は成功するはずです。**
```
student@setX-master:~$ kubectl expose deployment/nginx

service/nginx exposed 
```

**18. Service の設定を検証します。まずはService を確認してから、エンドポイントの情報を確認しましょう。現在のエンドポイントがClusterIP ではないことに注目しましょう。Calico がClusterIP を提供します。Endpoint はkubelet とkube-proxy が提供します。現在のエンドポイントのIP アドレスを書き留めておきます。次の例では192.168.1.5:80 です。後述の手順でこの情報を使います。**
```
student@setX-master:~$ kubectl get svc nginx

NAME    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
nginx   ClusterIP   10.100.61.122   <none>        80/TCP    3m
```
 
```
student@setX-master:~$ kubectl get ep nginx

NAME    ENDPOINTS        AGE
nginx   192.168.1.5:80   26s  
```

**19. コンテナがどのノードで起動しているかを確認します。そのノードにログインし、tcpdump を利用してtunl0 すなわち0 番目のトンネルのインターフェイスでトラフィックを確認しましょう。tcpdump は apt-get install を使用してインストールする必要があ
るかもしれません。この例では2 つめのノードです。cali で始まるインターフェイスでのトラフィックが見えるかもしれません。次のステップでcurl を実行している間は、このコマンドを実行したまままにしておきます。HTTP: HTTP/1.1 200 OK と、それと同じシーケンスに対するack レスポンスが表示されるはずです。**  
```
student@setX-master:~$ kubectl describe pod nginx-7cbc4b4d9c-d27xw | grep Node:

Node: master/10.128.0.5
```
```
student@setX-master:~$ sudo tcpdump -i tunl0

tcpdump: verbose output suppressed, use -v or -vv for full protocol...
listening on tunl0, link-type EN10MB (Ethernet), capture size...
< 省略>
```

**20. クラスタのIP アドレスの80 番ポートへアクセスします。nginx のデフォルトのウェルカムページが表示されるはずです。ENDPOINTS のIP アドレスに対しても同じ内容になるはずです。curl コマンドがタイムアウトする場合、Pod が他のノードで起動している可能性があります。同じコマンドをそのノードで実行すれば、成功するはずです。**
```
student@setX-master:~$ curl 10.100.61.122:80

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
< 省略>
```
```
student@setX-master:~$ curl 192.168.1.5:80
```

**21. Deployment を1 つのウェブサーバから3 つのウェブサーバにスケールアップしましょう。**
```
student@setX-master:~$ kubectl get deployment nginx

NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   1/1     1            1           12m
```
```
student@setX-master:~$ kubectl scale deployment nginx --replicas=3

deployment.apps/nginx scaled
```
```
student@setX-master:~$ kubectl get deployment nginx

NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   3/3     3            3           12m
```

**22. 現在のエンドポイントを確認しましょう。3 つあるはずです。もし前述のUP-TO-DATE が3 であるのにAVAILABLE が2 となっている場合は、数秒待ってもう一度試します。デプロイが完了するまで時間がかかっているかもしれません。**
```
student@setX-master:~$ kubectl get ep nginx

NAME    ENDPOINTS                                      AGE
nginx   192.168.0.3:80,192.168.1.5:80,192.168.1.6:80   7m40s
```

**23. nginx Deployment の内の一番古いPod を探し当てて、削除します。長い名前にはTab キーを活用しましょう。AGE フィールドを確認し、一番長く起動しているPod を見つけましょう。Pod を削除するとき、tcpdump を実行中のもう1 つのターミナルで何らかの動きがあるはずです。192.168.0 のアドレスを持つPod はおそらくコントロールプレーンにあり、192.168.1 のアドレスを持つPod はワーカノードにあるでしょう。**
```
student@setX-master:~$  kubectl get pod -o wide

NAME                     READY   STATUS    RESTARTS   AGE   IP                
nginx-1423793266-7f1qw   1/1     Running   0          14m   192.168.1.5
nginx-1423793266-8w2nk   1/1     Running   0          86s   192.168.1.6 
nginx-1423793266-fbt4b   1/1     Running   0          86s   192.168.0.3
```
```
student@setX-master:~$ kubectl delete po nginx-1423793266-7f1qw

pod "nginx-1423793266-7f1qw" deleted
```

**24. 1～2 分待ってから、もう一度Pod を確認しましょう。1 つが他より新しくなっているはずです。次の例では、AGE が4 分ではなく9 秒になっています。tcpdump がそのコンテナのveth インターフェイスを利用していたら、エラーが起こります。また、オブジェクトには短い名前を使用していることにも注意してください。**
```
student@setX-master:~$ kubectl get po

NAME                     READY   STATUS    RESTARTS   AGE                
nginx-1423793266-13p69   1/1     Running   0          9s 
nginx-1423793266-8w2nk   1/1     Running   0          4m1s  
nginx-1423793266-fbt4b   1/1     Running   0          4m1s
```

**25. 再度エンドポイントを確認します。先ほど確認したエンドポイントのIP アドレスが1 つ変わっています。どのPod を削除しても、Service が他のバックエンドのPod にトラフィックを転送するはずです。**
```
student@setX-master:~$ kubectl get ep nginx

NAME    ENDPOINTS                                      AGE
nginx   192.168.0.3:80,192.168.1.6:80,192.168.1.7:80   12m
```

**26. ClusterIP のIP アドレスやエンドポイントのIP アドレスのどれかを利用して、再度ウェブサーバへアクセスします。エンドポイントが変わってしまっても、依然としてウェブサーバへアクセスできます。なお、これらのIP アドレスに対するアクセスはクラスタ内からのみ可能です。完了したらctrl-c を使ってtcpdump コマンドを停止します。**
```
student@setX-master:~$ curl 10.100.61.122:80

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body
< 省略>
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter3/Lab3-5.md)
