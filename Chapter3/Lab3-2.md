# 課題3.2: クラスタの拡張

ターミナルをもう1 つ開き、2 つ目のノードに接続します。containerd とKubernetes のソフトウェアをインストールします。  
これらの多くはコントロールプレーンノードで行った手順です。  
本書では、各ノード用のコマンドを正しく実行できるように、ここで追加するノードにはsetX-worker をプロンプトとして使用します。  
注意：注意：プロンプトは、ユーザとコマンドを実行するシステムの両者を示します。正しいノードを把握するために、ターミナルセッションの色や文字を変更するのも有用です。  

**1. 以前と同じ手順で、2 つ目のノードに接続します。**

講師と対面でのトレーニングの場合、新しいノードには前と同じ.pemキーと、講師が提供する新しいIP アドレスを利用しましょう。見分けをつけるために、新しいターミナルウィンドウに1 つ目と異なる名前と色を与えるとよいでしょう。プロンプトが似ているためです。  
```
(a) student@setX-worker:~$ sudo -i
(b) root@setX-worker:~# apt-get update && apt-get upgrade -y
```
< 質問された場合、サービスの再起動を許可し、現在ローカルにインストールされているバージョンを維持します>  

```
(c) containerd エンジンをインストールし、依存ソフトウェアとともに起動します

root@setX-worker:~# sudo apt install curl apt-transport-https vim git wget gnupg2 software-properties-common ca-certificates uidmap -y

root@setX-worker:~# sudo swapoff -a
root@setX-worker:~# sudo modprobe overlay
root@setX-worker:~# sudo modprobe br_netfilter

root@setX-worker:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

root@setX-worker:~# sudo sysctl --system

root@setX-worker:~# sudo mkdir -p /etc/apt/keyrings
root@setX-worker:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

root@setX-worker:~# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

root@worker:~# apt-get update && apt-get install containerd.io -y
root@worker:~# containerd config default | tee /etc/containerd/config.toml
root@worker:~# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml
root@worker:~# systemctl restart containerd

(d) Kubernetes リポジトリを追加します

root@setX-worker:~#  vim /etc/apt/sources.list.d/kubernetes.list

deb http://apt.kubernetes.io/ kubernetes-xenial main

(e) ソフトウェアのための GPG 鍵を取得します

root@setX-worker:~#  curl -s \
https://packages.cloud.google.com/apt/doc/apt-key.gpg \
| apt-key add -


root@setX-worker:~# apt-get update

(f) リポジトリを更新し、Kubernetes ソフトウェアをインストールします。コントロールプレーンのバージョンと必ず一致させて下さい。

root@setX-worker:~# apt-get install -y \
kubeadm=1.26.1-00 kubelet=1.26.1-00 kubectl=1.26.1-00


(g) システム更新時にバージョンが維持されるようにします

root@setX-worker:~# apt-mark hold kubeadm kubelet kubectl

```

**2. コントロールプレーンサーバのIP アドレスを見つけます。**

インターフェイスの名前は、ノードが起動している場所によって変わります。例ではGCE で起動しているので、このノードのプライマリインターフェイスはens4 です。受講者のインターフェイスの名前は異なるかもしれません。出力内容からマスタノードのIP アドレスは10.128.0.3 だということがわかります。 
```
student@setX-master:~$ hostname -i

10.128.0.3
``` 
```
student@setX-master:~$ ip addr show ens4 | grep inet

  inet 10.128.0.3/32 brd 10.128.0.3 scope global ens4  
  inet6 fe80::4001:aff:fe8e:2/64 scope link  
```

**3. この時点で、join コマンドをコントロールプレーンノードからコピー＆ペーストしましょう。**

このコマンドは2 時間のみ有効なので、将来的には独自のjoin をビルドしノードを追加する必要があります。マスタノードのトークンを見つけます。このトークンはデフォルトで2 時間有効です。有効期限を過ぎてしまったり、トークンを紛失した場合、次の次に示すsudo kubeadm token create コマンドで新しいトークンを作成できます。  

```
studentsetX-master:~$ sudo kubeadm token list

TOKEN                   TTL EXPIRES              USAGES                 DESCRIPTION  EXTRA GROUPS
bml44w.3owxl50rrtymamt7 2h  2021-05-27T18:49:41Z authentication,signing <none>       system:bootstrappers:kubeadm:default-node-token  
```

**4. 2 時間以上後にノードを追加することになり、join コマンドの一部として使用するため新しくトークンを作成することになったと想定しましょう。。cri-o を利用している場合、Docker が見つからない警告が表示されるかもしれません。**
```
studentsetX-master:~$ sudo kubeadm token create

27eee4.6e66ff60318da929
```

**5. ノードを安全にクラスタに追加することを保証するために、マスタノードでDiscovery Token CA Cert Hash を作成し、利用しましょう。これはマスタノード、あるいはCA ファイルのコピーがある場所で実行します。長い出力内容を表示するはずです。**

PDF からコピーペーストした場合、コマンド末尾の CA 証明書 (^) とシングルクォート (') が問題になる場合があります。  

```
studentsetX-mster:~$ openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'
```
6d541678b05652e1fa5d43908e75e67376e994c3483d6683f2a18673e5d2a1b0  

**6. ワーカノード上で、マスタサーバ用のローカルDNS エイリアスを追加しましょう。/etc/hostsファイルを編集し、マスタのIP アドレスを追加してk8scp という名前を割り当てます。**
```
root@setX-worker:~# vim /etc/hosts

10.128.0.3 k8scp        #<-- この行を追加  
127.0.0.1 localhost
...
```

**7. トークンとハッシュ（この場合はsha256:hash）を利用して、ワーカとなる2 つ目のノードからクラスタにjoin しましょう。コントロールプレーンサーバのプライベートなIP アドレスと6443 番ポートを利用します。コントロールプレーンノードのkubeadm init の出力内容に例がありますので、残っていれば使用しましょう。**
```
root@setX-worker:~# kubeadm join --token 27eee4.6e66ff60318da929 k8scp:6443 --discovery-token-ca-cert-hash sha256:6d541678b05652e1fa5d43908e75e67376e994c3483d6683f2a18673e5d2a1b0

[preflight] Running pre-flight checks  
[WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/  
[preflight] Reading configuration from the cluster...  
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'  
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.15" ConfigMap in the kube-system namespace  
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"  
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"  
[kubelet-start] Activating the kubelet service  
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...  
This node has joined the cluster:  
* Certificate signing request was sent to apiserver and a response was received.  
* The Kubelet was informed of the new secure connection details.  
Run 'kubectl get nodes' on the control-plane to see this node join the cluster.  
```

**8. ワーカノードでkubectl コマンドを実行します。失敗するはずです。その理由は、ローカルの.kube/configファイルにクラスタと認証キーがないためです。**
```
root@setX-worker:~# exit
student@setX-worker:~$ kubectl get nodes

The connection to the server localhost:8080 was refused  
- did you specify the right host or port?
```
  
```
student@setX-worker:~$ ls -l .kube  

ls: cannot access '.kube': No such file or directory  
```



以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter3/Lab3-3.md)
