# 課題4.2: CPU とメモリを制限する

**概要**

前回の演習で作成したクラスタを続けて利用していきます。リソースの制限やnamespace について見ていきます。また、複雑なデプロイを実践することで、アーキテクチャや関係性についての理解が深まります。  
SSH あるいはPuTTY で、前回の演習でインストールしたノードに接続しましょう。コンテナ上でstress というアプリケーションをデプロイしてから、リソース制限を利用してアプリケーションがアクセスできるリソースを制限していきます。  


**1. 負荷を生成するstress というコンテナにhog という名前をつけてDeployment を作成します。コンテナが1 つ起動していることを確認します。**
```
student@setX-master:~$ kubectl create deployment hog --image vish/stress

deployment.apps/hog created
```
```
student@setX-master:~$ kubectl get deployments

NAME   READY   UP-TO-DATE   AVAILABLE   AGE
hog    1/1     1            1           13s

```

**2. describe コマンドを利用して詳細を確認してから、YAML フォーマットで出力内容を確認します。リソースの利用を制限する設定が存在していないはずです。代わりに、{} のような空の波括弧があります。**
```
student@setX-master:~$ kubectl describe deployment hog

Name: hog
Namespace: default
CreationTimestamp: Thu, 23 Feb 2023 10:15:53 +0000
Labels: app=hog
Annotations: deployment.kubernetes.io/revision: 1
< 省略 >
```
```
student@setX-master:~$ kubectl get deployment hog -o yaml

apiVersion: apps/v1
kind: Deployment
Metadata:
< 省略 >
  template:
  metadata:
    creationTimestamp: null
    labels:
      app: hog
  spec:
    containers:
    - image: vish/stress
      imagePullPolicy: Always
      name: stress
      resources: {}
      terminationMessagePath: /dev/termination-log
< 省略 >
```

**3. YAML フォーマットの出力を利用して自分で使用する設定ファイルを作成します。**
```
student@setX-master:~$ kubectl get deployment hog -o yaml > hog.yaml
```

**4. status の出力とcreationTimestamp などを除くのが、おそらくはよいでしょう。さらに、次のようにメモリの制限を追加します。**
```
student@setX-master:~$ vim hog.yaml
```
![chap4-1](/uploads/29eede3df1e5952ae036ba772636d5aa/chap4-1.png)

[hog.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter4/labfiles/hog-0.yaml)



**5. 今修正したファイルを利用してDeployment を置き換えます。**
```
student@setX-master:~$ kubectl replace -f hog.yaml

deployment.apps/hog replaced
```

**6. 修正内容が反映されていることを確認します。リソースの制限が表示されるはずです。**
```
student@setX-master:~$ kubectl get deployment hog -o yaml

< 省略>
        resources:
          limits:
            memory: 4Gi
          requests:
            memory: 2500Mi
        terminationMessagePath: /dev/termination-log
< 省略>
```

**7. hog コンテナのログを表示し、割り当てられたメモリの量を確認します。**
```
student@setX-master:~$ kubectl get po

NAME                   READY   STATUS    RESTARTS   AGE
hog-64cbfcc7cf-lwq66   1/1     Running   0          2m

```
```
student@setX-master:~$ kubectl logs hog-64cbfcc7cf-lwq66

I1102 16:16:42.638972 1 main.go:26] Allocating "0" memory, in "4Ki" chunks, with a 1ms sleep between allocations  
I1102 16:16:42.639064 1 main.go:29] Allocated "0" memory 
```

**8. コントロールプレーンノードとセカンドノードの両方にアクセスするため、2 つ目のターミナルと3 つ目のターミナルを開きます。リソースの利用状況を見るためにtop コマンドを実行します。この時点では、異常なリソース利用はないはずです。dockerd とtop がほぼ同量のリソースを利用していることがわかるでしょう。一方、stress コマンドは特筆するほどのリソースを利用していません。。kubectl get events を使って、消費量のリソースが多すぎる場合に、Pod が追い出されているかどうかを確認して下さい。**

**9. hog 設定ファイルを修正します。stress がCPU とメモリを消費するように引数を追加します。args: の行のインデント幅をresources: の行のインデント幅と揃えます。**
```
student@setX-master:~$ vim hog.yaml
```

![chap4-2](/uploads/9dcb0c5c0a4c6102605f83f2daf5fbe7/chap4-2.png)

[hog.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter4/labfiles/hog-1.yaml)


**10. Deployment を再生成します。直ちにCPU の消費量が増加し、stress プログラムに割り当てられるメモリが100M ずつ増加していくことが、実行中のtop コマンドによりわかるはずです。コンテナはどちらのノードにもデプロイされる可能性があるので両方のノードを確認します。メモリや CPU が少ないノードでは、問題が発生する可能性があるので注意が必要です。コントロールプレーンノードのインフラストラクチャ Pod に障害が発生する問題等です。標準 Pod がエラーなく動作するように、使用するリソースの量を調整します。**
```
student@setX-master:~$ kubectl delete deployment hog

deployment.apps "hog" deleted
```
```
student@setX-master:~$ kubectl create -f hog.yaml

deployment.apps/hog created
```

**リソース使用量が増加していない場合**

メモリとCPU の使用量が増加していない場合、コンテナ内で問題が発生していた可能性があります。Kubernetes はコンテナが実行中と表示しているにもかかわらず、実際のワークロードが障害を起こしている可能性があります。あるいは、コンテナ自体が障害を起こしているかもしれません。例えば、パラメータの指定が抜けている場合、コンテナがパニックし、次のような内容を出力する可能性があります。  

```
student@setX-master:~$ kubectl get pod

NAME                   READY   STATUS    RESTARTS   AGE
hog-1985182137-5bz2w   0/1     Error     1          5s

```
```
student@setX-master:~$ kubectl logs hog-1985182137-5bz2w

panic: cannot parse '150mi': unable to parse quantity's suffix

goroutine 1 [running]:
panic(0x5ff9a0, 0xc820014cb0)
      /usr/local/go/src/runtime/panic.go:481 +0x3e6
k8s.io/kubernetes/pkg/api/resource.MustParse(0x7ffe460c0e69, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
      /usr/local/google/home/vishnuk/go/src/k8s.io/kubernetes/pkg/api/resource/quantity.go:134 +0x287
main.main()
      /usr/local/google/home/vishnuk/go/src/github.com/vishh/stress/main.go:24 +0x43
```

次は、誤ったパラメータの例です。コンテナは起動していますが、メモリを割り当てていません。本来は、YAML ファイルで設定したリソース割り当てが表示されるはずです。  

```
student@setX-master:~$ kubectl get po

NAME                   READY   STATUS    RESTARTS   AGE
hog-1603763060-x3vnn   1/1     Running   0          8s  
```
```
student@setX-master:~$ kubectl logs hog-1603763060-x3vnn

I0927 21:09:23.514921 1 main.go:26] Allocating "0" memory, in "4ki" chunks, with a 1ms sleep between allocations  
I0927 21:09:23.514984 1 main.go:39] Spawning a thread to consume CPU  
I0927 21:09:23.514991 1 main.go:39] Spawning a thread to consume CPU  
I0927 21:09:23.514997 1 main.go:29] Allocated "0" memory 
```


以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter4/Lab4-3.md)
