# 課題13.1: ログファイルの場所確認

**概要**

さまざまなログファイルやコマンドの出力内容に加え、journalctl を利用してノードのログを取得することもできます。ログファイルの一般的な場所と、コンテナログを取得するためのコマンドを紹介します。ロギングの方法は他にもあります。例えば、Pod内のコンテナのログを読み込むことに特化したsidecar コンテナの利用です。
<br>  
Kubernetes では、クラスタ全体にわたるロギング機能をまだ提供していません。通常、Fluentd(https://fluentd.org/) などを利用します。Fluentd はKubernetes 同様、CNCF のプロジェクトの一つです。  
<br>
<br>
次のログファイルとウェブサイトに目を通しましょう。サーバプロセスがノードレベルからコンテナレベルでの実行になるため、ロギングもコンテナレベルで行われます。  

**1. systemd ベースのKubernetes クラスタを利用している場合、ローカルKubernetes エージェントのkubelet のノードレベルのログを確認しましょう。出力されるログは各ノードごとに異なります。**
```
student@setX-master:~$ sudo journalctl -u kubelet |less

< 省略>
```

**2. Kubernetes の主要なプロセスはコンテナ内で実行されています。コンテナからもPod からも参照できます。**

find コマンドを利用してkube-apiserver のログを見つけましょう。このコマンドの出力内容は非常に長く、例とは異なるはずです。  
```
student@setX-master:~$ sudo find / -name "*apiserver*log"

/var/log/containers/kube-apiserver-cp_kube-system_kube-apiserver-423d25701998f68b503e64d41dd786e657fc09504f13278044934d79a4019e3c.log
```

**3. ログファイルを確認します。**
```
student@setX-master:~$ sudo less /var/log/containers/kube-apiserver-cp_kube-system_kubeapiserver-423d25701998f68b503e64d41dd786e657fc09504f13278044934d79a4019e3c.log

< 省略>
```

**4. coredns やkube-proxy などの他のクラスタエージェントのログファイルも探して確認しましょう。**

**5. systemd を利用しないKubernetes クラスタの場合、マスタノードでログファイルを確認できます。**

(a) /var/log/kube-apiserver.log  
Kubernetes API ログ  
(b) /var/log/kube-scheduler.log  
Kubernetes Scheduler ログ  
(c) /var/log/kube-controller-manager.log  
ReplicationController コントローラログ  

**6. /var/log/containers**

さまざまなコンテナログ  

**7. /var/log/pods/**

実行中のPod のログファイル  

**8. ワーカノードのファイル（systemd でないシステムの場合）**

(a) /var/log/kubelet.log  
kubelet ログ  
(b) /var/log/kube-proxy.log  
kube-proxy ログ  

**9. 追加資料：**

https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/  
https://kubernetes.io/docs/tasks/debug-application-cluster/determine-reason-pod-failure/  


以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter13/Lab13-2.md)

