# 課題13.3: モニタリングとメトリクス用のツールを追加

Heapster の非推奨化に伴い、新しい統合されたMetrics Server の開発およびデプロイが進んでいます。CNCF.ioのPrometheus プロジェクトは、インキュベーション段階から卒業段階まで成長し、メトリクス収集に多く使われているため、利用を検討すべきでしょう。

**[メトリクスの設定]**

**重要**

metrics-server はDocker と通信します。cri-o を利用している場合、ログはエラーとなり、メトリクスを収集できません。

**1. まず、ソフトウェアをクローンしましょう。git コマンドは既にインストール済みでしょう。なければ、インストールしましょう。**
```
student@setX-master:~$ git clone https://github.com/kubernetes-incubator/metrics-server.git

< 省略>
```

**2. ソフトウェアが更新されている場合があるため、更新情報をREADME.md を読んで確認すると良いでしょう。**
```
student@setX-master:~$ cd metrics-server/ ; less README.md

< 省略>
```

**3. metrics-server をデプロイしましょう。新しいバージョンがリリースされている場合、手順や作成されるオブジェクトに変更がある可能性があるため、注意してください。components.yaml を使ってオブジェクトを作成します。コマンドを一行で入力する場合、バックスラッシュは不要です。**
```
student@setX-master:~$ kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml

Warning: resource serviceaccounts/metrics-server is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
serviceaccount/metrics-server configured
Warning: resource clusterroles/system:aggregated-metrics-reader is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
<省略>
deployment.apps/metrics-server created
apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io created

```

**4. namespace kube-system に作成されている、metrics-serverは　0/1　Running状態になっています。**
```
student@setX-master:~$ kubectl -n kube-system get pods

NAME                                       READY   STATUS    RESTARTS   AGE
< 省略>  
kube-proxy-ld2hb                           1/1     Running   0          2d21h  
kube-scheduler-u16-1-13-1-2f8c             1/1     Running   0          2d21h  
metrics-server-fc6d4999b-b9rjj             0/1     Running   0          20s           
```

**5. Deployment metrics-server を編集し、セキュアでないTLS を有効化しましょう。デフォルトの証明書はx509 の自己署名で、デフォルトでは信頼されていません。本番環境では、証明書を設定し、置き換えることを検討しましょう。このソフトウェアは変化が早いので、他の問題が発生するかもしれません。いくつかのプラットフォームでは、kubelet-preferred-address-typesの行が必要となるようです。**

```
追加するのは42行目です。
kubelet-preferred-address-typesが既にある場合はkubelet-insecure-tlsのみ追加します。なければ2行追加します。 
  
student@setX-master:~$ kubectl -n kube-system edit deployment metrics-server
```

![chap13-1](/uploads/d5e92783fc94605a00518e83606960f9/chap13-1.png)

[metric-server追加](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter13/labfiles/metrics-server-add.yaml)


**6. Metrics Server のPod が起動していて、エラーが表示されていないことを確認しましょう。数分時間がかかるかもしれません。はじめに、コンテナが待ち受け状態であることを示すログが表示されているはずです。バージョンによってはログが少し異なるかもしれません。**
```
student@setX-master:~$ kubectl -n kube-system get pods

NAME                                       READY   STATUS    RESTARTS   AGE
< 省略>  
kube-proxy-ld2hb                           1/1     Running   0          2d21h  
kube-scheduler-u16-1-13-1-2f8c             1/1     Running   0          2d21h  
metrics-server-fc6d4999b-b9rjj             1/1     Running   0　　　　　10s　　

student@setX-master:~$ kubectl -n kube-system logs metrics-server<TAB>

I0207 14:08:13.383209     1 serving.go:312] Generated self-signed cert (/tmp/apiserver.crt, /tmp/apiserver.key)  
I0207 14:08:14.078360     1 secure_serving.go:116] Serving securely on  [::]:4443
<省略>

```

**7. Pod とノードのメトリクスを確認し、メトリクスが正常に動作していることを確認しましょう。環境によってPod の出力は異なります。メトリクスを収集し、エラーが表示されなくなるまで数分かかるかもしれません。**
```
student@setX-master:~$ sleep 120 ; kubectl top pod --all-namespaces

NAMESPACE         NAME                                       CPU(cores)   MEMORY(bytes)
kube-system       calico-kube-controllers-7b9dcdcc5-qg6zd    2m           6Mi  
kube-system       calico-node-dr279                          23m          22Mi  
kube-system       calico-node-xtvfd                          21m          22Mi  
kube-system       coredns-5644d7b6d9-k7kts                   2m           6Mi  
kube-system       coredns-5644d7b6d9-rnr2v                   3m           6Mi  
< 省略>  
```
```
student@setX-master:~$ kubectl top nodes

NAME          CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
cp            228m         11%    2357Mi          31%  
worker        76m          3%     1385Mi          18%  
```

**8. 先の演習で生成したキーを利用して、API サーバを検証しましょう。デフォルトディレクトリにいる事を確認してください。**
```
student@setX-master:~$ cd ; pwd
student@setX-master:~$ curl --cert ./client.pem --key ./client-key.pem --cacert ./ca.pem https://k8scp:6443/apis/metrics.k8s.io/v1beta1/nodes

{
  "kind": "NodeMetricsList",
  "apiVersion": "metrics.k8s.io/v1beta1",
  "metadata": {
    "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes"
  },
  "items": [
    {
      "metadata": {
        "name": "u16-1-13-1-2f8c",
        "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes/u16-1-13-1-2f8c",
        "creationTimestamp": "019-01-10T20:27:00Z"
      },
      "timestamp": "2019-01-10T20:26:18Z",
      "window": "30s",
      "usage": {
        "cpu": "215675721n",
        "memory": "2414744Ki"
      }
    },
    < 省略>   
```

**[ダッシュボードの設定]**

ダッシュボードの見映えは良いですが、あまり利用されていないツールです。ツールを一番改善する能力のある人はCLI を使う傾向があるため、欲しい機能が欠けているかもしれません。  
はじめのいくつかのコマンドについては詳細を説明しませんが、必要に応じて以前の内容を参照して下さい。  

[参考](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter13/Lab13-4.md)


**1.  https://artifacthub.io/で helm organization が提供するkubernetes-dashboard Chart を検索します。**

**2. Chart を取得し、values.yamlファイルを編集します。**

![chap13-2](/uploads/6c94d57befbdc3864d61989166041ef5/chap13-2.png)

**3. Chart をインストールし、dashboard と命名します。**

**4. このバージョンの Helm Chart では、デフォルトで全てのリソースへのアクセスが許可されていません。ダッシュボードに対して完全な管理者権限を与えます。本番環境ではより権限を制限する場合もあります。ダッシュボードはデフォルトでdefault ネームスペースで動作します。まず Chart に利用した名前をもとにサービスアカウントを見つけます。サービスアカウントについてはセキュリティの章でより詳しく説明します。**
```
student@setX-master:~$ kubectl get serviceaccounts

NAME                           SECRETS AGE
dashboard-kubernetes-dashboard 1       6m
default                        1       2d21h
myingress-ingress-nginx        1       42h
```
```
student@setX-master:~$ kubectl create clusterrolebinding dashaccess --clusterrole=cluster-admin --serviceaccount=default:dashboard-kubernetes-dashboard

clusterrolebinding.rbac.authorization.k8s.io/dashaccess created
```

**5. ローカルマシンのブラウザで、https://ノードのパブリックのIPアドレス:先ほど確認したポート番号 を開いてください。**

接続がセキュアでないというメッセージを表示するはずです。お使いのブラウザがFirefox の場合はAdvanced ボタンを押し、次にAddException...、最後にConfirm Security Exception を押します。ブラウザによっては選択肢が提供されない場合があります。
何も選択肢が表示されない場合は他のブラウザを試して下さい。Kubernetes Dashboard が見えるはずです。また curl によ
ってパブリック IP が検出できるでしょう。
```
student@setX-master:~$ curl ifconfig.io

35.231.8.178
```

![chap13-3](/uploads/628897facdece83fd296c0d3f287d318/chap13-3.png)


**6. Token を利用してダッシュボードにアクセスします。**

RBAC では適切なトークンを利用する必要があります。この場合はkubernetes-dashboard-token です。トークンを見つけ、コピーし、ログインページに貼り付けましょう。Secret 名はハッシュを探すより、Tab キーの補完を使うと便利です。　　  
```
student@setX-master:~$ kubectl create token dashboard-kubernetes-dashboard

eyJlxvezoLAilithbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZX
JuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY
291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi1wbW04NCIsImt1YmVybmV0ZXMuaW8vc2Vydmlj
ZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2Vydml
jZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjE5MDY4ZDIzLTE1MTctMTFlOS1hZmMyLTQyMDEwYThlMDAwMyIsInN1Yi
I6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.aYTUMWr290pjt5i32rb8
qXpq4onn3hLhvz6yLSYexgRd6NYsygVUyqnkRsFE1trg9i1ftNXKJdzkY5kQzN3AcpUTvyj_BvJgzNh3JM9p7QMjI8LHTz4TrRZ
rvwJVWitrEn4VnTQuFVcADFD_rKB9FyI_gvT_QiW5fQm24ygTIgfOYd44263oakG8sL64q7UfQNW2wt5SOorMUtybOmX4CXNUYM8
G44ejEtv9GW5OsVjEmLIGaoEMX7fctwUN_XCyPdzcCg2WOxRHahBJmbCuLz2SSWL52q4nXQmhTq_L8VDDpt6LjEqXW6LtDJZGjVC
s2MnBLerQz-ZAgsVaubbQ
```

![chap13-4](/uploads/2d30ef6a7bae06297f541904bf2c5bf1/chap13-4.png)


**7. 時間が許す限り、左側のメニューを利用し、さまざまなセクションを見てみましょう。GUI 経由でリソースを見るためには、Pod ビューはdefault namespace のものなのでkube-system namespace に切り替えるか新しいDeployment を作成しましょう。デプロイをスケールアップ・スケールダウンして、GUI の応答性を見てみましょう。画面のデザインは異なるかもしれません。**

![chap13-5](/uploads/0d01358c90ca51f17da78ded460c12d4/chap13-5.png)


以上
