# 課題14.1: Custom Resource Definition の作成

**概要**

CustomResourceDefinitions (CRD)の使用は、新しいオブジェクトや演算子を展開するための一般的な方法となってきました。演算子の作成は本講座の範囲外ですが、簡単に言うとspec とオブジェクトの状態を比較し、一致するまで状態の修正を行う監視ループです。コントローラの作成についてのわかりやすいディスカッションはこちらで参照できます:  
https://operatorframework.io/  

まず既存のCRD を調べて、その後、特定の動作を行わない単純なCRD を作成します。オブジェクトがAPI から利用可能となり、コマンドに応答していることを確認します。  

**1. 既存のCRD を確認します。**
```
student@setX-master:~$ kubectl get crd --all-namespaces

NAME                                                  CREATED AT
bgpconfigurations.crd.projectcalico.org               2020-04-19T17:29:02Z  
bgppeers.crd.projectcalico.org                        2020-04-19T17:29:02Z  
blockaffinities.crd.projectcalico.org                 2020-04-19T17:29:02Z  
< 省略>  
```

**2. 名前からわかるように、これらのCRD はすべて、ネットワーク・プラグインのCalico で動いています。クラスタを初期化したときに使用したcalico.yamlファイルを見て、これらのオブジェクトがどのように作成されたかを確認します。また、CRD テンプレートも確認します。**
```
student@setX-master:~$ less calico.yaml

< 省略>  
---  
# Source: calico/templates/kdd-crds.yaml  
apiVersion: apiextensions.k8s.io/v1beta1  
kind: CustomResourceDefinition  
metadata:  
  name: bgpconfigurations.crd.projectcalico.org  
< 省略>
```

**3. さて、いくつかの例を見てきましたので、新しいYAML ファイルを作成します。**
```
student@setX-master:~$ vim crd.yaml
```
![chap14-1](/uploads/53c838e26080859f7488b4545e3cf299/chap14-1.png)

[crd.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter14/labfiles/crd.yaml)


**4. 新しいリソースをクラスタに追加します。**
```
student@setX-master:~$ kubectl create -f crd.yaml

customresourcedefinition.apiextensions.k8s.io/crontabs.stable.example.com created
```

**5. リソースの存在を確認し、describe コマンドで詳細を確認しましょう。出力のなかほどに新しい行があるはずです。describe の出力がこれまで見てきた他のオブジェクトと異なることにお気づきでしょうか。**
```
student@setX-master:~$ kubectl get crd

NAME                        CREATED AT
< 省略>
crontabs.stable.example.com 2021-06-13T03:18:07Z
< 省略>
```
```
student@setX-master:~$ kubectl describe crd crontab<Tab>

Name:          crontabs.stable.example.com
Namespace:
Labels:        <none>
Annotations:   <none>
API Version:   apiextensions.k8s.io/v1
Kind:          CustomResourceDefinition
< 省略>
```

**6. 新しいAPI リソースができたため、そのタイプの新規オブジェクトが作成可能です。デモ用のため実行されませんが、Linux のcrontab のような役割を担います。**
```
student@setX-master:~$ vim new-crontab.yaml
```
![chap14-2](/uploads/5c01427177c3686879e331070725b563/chap14-2.png)

[new-crontab.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter14/labfiles/new-crontab.yaml)


**7. 新しいオブジェクトを作成し、長い名称と略称の両方でリソースを確認します。**
```
student@setX-master:~$ kubectl create -f new-crontab.yaml

crontab.training.lfs458.com/new-cron-object created
```
```
student@setX-master:~$ kubectl get CronTab

NAME              AGE
new-cron-object   29s
```
```
student@setX-master:~$ kubectl get ct

NAME              AGE
new-cron-object   29s
```
```
student@setX-master:~$ kubectl describe ct

Name:         new-cron-object
Namespace:
Labels:       <none>
Annotations:  <none>
API Version:  stable.example.com/v1
Kind:         CronTab

< 省略>  
Spec:
  Cron Spec:  */5 * * * *
  Image:      some-cron-image
Events:       <none>
```

**8. リソースのクリーンアップを行うため、CRD を削除します。これにより、CRD を利用しているすべてのエンドポイントとオブジェクトも削除されます。**
```
student@setX-master:~$ kubectl delete -f crd.yaml

 customresourcedefinition.apiextensions.k8s.io "crontabs.stable.example.com" deleted
```
```
student@setX-master:~$ kubectl get ct

Error from server (NotFound): Unable to list "stable.example.com/v1,Resource=crontabs": the server could not find the requested resource(get crontabs.stable.example.com)
```


以上
