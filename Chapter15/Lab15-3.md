# 課題15.3: Admission Controllers

リクエストがAPI サーバに送信される直前にAdmission Controller が実行されます。デフォルトのストレージクラス、リソースクォータ、またはセキュリティの設定などの確認が行われます。より新しい機能(v1.7.x) として、ランタイム時に新規コントローラの読み込みと設定が可能な動的コントローラが存在します。

**1. Admission Controller の現在の設定を確認しましょう。**

Kubernetes のこれまでのバージョンとは違い、実行時にコントローラを設定するのではなく、サーバに組み込まれています。実行時に設定するのは、利用するコントローラではなく、プラグインの有効化や無効化です。  
```
student@setX-master:~$ sudo grep admission /etc/kubernetes/manifests/kube-apiserver.yaml
```
\- --enable-admission-plugins=NodeRestriction  

以上  
