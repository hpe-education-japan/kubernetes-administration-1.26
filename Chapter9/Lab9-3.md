# 課題9.3: CoreDNS を扱う

**1. CoreDNS によって、IP アドレスの代わりにホスト名を利用することが可能です。少し前の手順でservice-lab NodePort を
Accounting namespace に作成しました。Ubuntu を使ってテスト用の新しいPod を作成します。作成するPod はnettool とい
う名前にします。**
```
student@setX-master:~$ vim nettool.yaml
```

![chap9-6](/uploads/040ba2683cbff57446601823b3cb20a8/chap9-6.png)

[nettool.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter9/labfiles/nettool.yaml)


**2. Pod を作成し、コンテナにログインします。**
```
student@setX-master:~$ kubectl create -f nettool.yaml

pod/ubuntu created
```
```
student@setX-master:~$ kubectl exec -it ubuntu -- /bin/bash
```

**コンテナ内**

**(a) DNS およびネットワークを調査するために、いくつかのツールを追加します。インストールにあたって、地理的なエ
リアとタイムゾーンの情報が尋ねられた場合、はじめにGeographic area: 6 (Asia)、そしてTime zone: 79　(Tokyo) を入力します。**
```
root@ubuntu:/# apt-get update ; apt-get install curl dnsutils -y
```

**(b) dig コマンドをオプション無しで利用します。ルートネームサーバと、応答したDNS サーバのIP アドレスなどの情
報が確認できるはずです。**
```
root@ubuntu:/# dig

; <<>> DiG 9.16.1-Ubuntu <<>>
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54028
;; flags: qr rd ra; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 1

< 省略>

;; Query time: 15 msec
;; SERVER: 10.96.0.10#53(10.96.0.10)
;; WHEN: Mon Dec 14 16:23:01 JST 2020
;; MSG SIZE  rcvd: 431
```

**(c) また、/etc/resolv.confも見てみましょう。ネームサーバや、完全修飾ドメイン名(FQDN) でない場合に検索するデフ
ォルトドメインが設定されています。内容から、一番目のエントリがdefault.svc.cluster.local. であることがわかります。**
```
root@ubuntu:/# cat /etc/resolv.conf

nameserver 10.96.0.10
search default.svc.cluster.local svc.cluster.local cluster.local
c.endless-station-188822.internal google.internal
options ndots:5
```

**(d) dig コマンドを使って、DNS サーバに関するより多くの情報を見てみましょう。-x オプションを使って、既知のIP か
らFQDN を取得することができます。.kube-system.svc.cluster.local. を使ったドメイン名が、default ではなくPod のnsmespace に対応することに注目してください。また、kube-dns という名前が、Pod の名前ではなく、Service の名前であることに注意してください。**
```
root@ubuntu:/# dig @10.96.0.10 -x 10.96.0.10

...
;; QUESTION SECTION:
;10.0.96.10.in-addr.arpa. IN PTR

;; ANSWER SECTION:
10.0.96.10.in-addr.arpa. 30 IN PTR kube-dns.kube-system.svc.cluster.local.

;; Query time: 0 msec
;; SERVER: 10.96.0.10#53(10.96.0.10)
;; WHEN: Thu Aug 27 23:39:14 CDT 2020
;; MSG SIZE rcvd: 139
```

**(e) 作成したservice-lab Service の名前と、このService が作成されたnamespace を思い出しましょう。これらの
情報をつかって、FQDN を作成し、公開されたPod を見てみましょう。**
```
root@ubuntu:/# curl service-lab.accounting.svc.cluster.local.


<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
...
```

**(f) Service の名前を使って、デフォルトページを見てみましょう。nettool がdefault namespace にあるため、失敗す
るはずです。**
```
root@ubuntu:/# curl service-lab

curl: (6) Could not resolve host: service-lab
```

**(g) accounting namespace を名前に追加して、再度挑戦してみましょう。異なるnamespace であっても、通信が到
達することができます。**
```
root@ubuntu:/# curl service-lab.accounting

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
< 省略>
```
  
**(h) コンテナから抜けて、kube-system namespace 内で可動するService を確認します。出力内容から、kube-dns
Service がDNS ServerIP を持ち、またDNS が利用するポートを公開してることがわかります。**
```
root@ubuntu:/# exit
```
**コンテナ内ここまで**

```
student@setX-master:~$ kubectl -n kube-system get svc

NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   42h
```

**3. Service を詳しく調査します。中でも、Service が通信するPod を定義するために利用されている、Selector に注目しましょう。**
```
student@setX-master:~$ kubectl -n kube-system get svc kube-dns -o yaml

...
  labels:
    k8s-app: kube-dns
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: KubeDNS
...
  selector:
    k8s-app: kube-dns
  sessionAffinity: None
  type: ClusterIP
...
```

**4. 全てのnamespace の中から、同じラベルを持つPod を探します。coredns を含むKubernetes システムに必要なPod 全てが、このラベルを持っていることがわかります。**
```
student@setX-master:~$ kubectl get pod -l k8s-app --all-namespaces

NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-5447dc9cbf-275fs   1/1     Running   0          41h
kube-system   calico-node-6q74j                          1/1     Running   0          43h
kube-system   calico-node-vgzg2                          1/1     Running   0          42h
kube-system   coredns-f9fd979d6-4dxpl                    1/1     Running   0          41h
kube-system   coredns-f9fd979d6-nxfrz                    1/1     Running   0          41h
kube-system   kube-proxy-f4vxx                           1/1     Running   0          41h
kube-system   kube-proxy-pdxwd                           1/1     Running   0          41h
```

**5. coredns Pod のうち一つを選んで詳細を見てみましょう。Pod のspec に目を通してみると、利用されているイメージなどの全ての設定情報が確認できます。ConfigMap に由来する設定が見つかるはずです。**
```
student@setX-master:~$ kubectl -n kube-system get pod coredns-f9fd979d6-4dxpl -o yaml

spec:
  containers:
  - args:
    - -conf
    - /etc/coredns/Corefile
    image: k8s.gcr.io/coredns:1.7.0
...
    volumeMounts:
    - mountPath: /etc/coredns
      name: config-volume
      readOnly: true
...
  volumes:
  - configMap:
      defaultMode: 420
      items:
      - key: Corefile
        path: Corefile
      name: coredns
    name: config-volume
...
```

**6. kube-system namespace にあるConfigMap を確認します。**
```
student@setX-master:~$ kubectl -n kube-system get configmaps

NAME                                 DATA   AGE
calico-config                        4      43h
coredns                              1      43h
extension-apiserver-authentication   6      43h
kube-proxy                           2      43h
kubeadm-config                       2      43h
kubelet-config-1.20                  1      43h
kubelet-config-1.24                  1      41h
```

**7. coredns ConfigMap の詳細を見てみましょう。cluster.local ドメインが表示されることに注意してください。**
```
student@setX-master:~$ kubectl -n kube-system get configmaps coredns -o yaml

apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . /etc/resolv.conf {
           max_concurrent 1000
        }
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
...
```

**8. 多くのオプションやゾーンファイルを設定することが可能ですが、まずは簡単な設定変更を行いましょう。test.ioがcluster.local にリダイレクトされるように、rewrite の定義を追加してください。各行の詳細は、coredns.ioにて確認す
ることができます。**
```
student@setX-master:~$ kubectl -n kube-system edit configmaps coredns

apiVersion: v1
data:
  Corefile: |
    .:53 {
        rewrite name regex (.*)\.test\.io {1}.default.svc.cluster.local #<-- この行を追加
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . /etc/resolv.conf {
           max_concurrent 1000
        }
        cache 30
        loop
        reload
        loadbalance
  }      
```
**9. coredns Pod を削除して、更新されたConfigMap を再読込みさせます。**
```
student@setX-master:~$ kubectl -n kube-system delete pod coredns-f9fd979d6-s4j98 coredns-f9fd979d6-xlpzf

pod "coredns-f9fd979d6-s4j98" deleted
pod "coredns-f9fd979d6-xlpzf" deleted
```

**10. 新しいWeb サーバと、アドレスの動作を検証するためのClusterIP Service を作成します。逆引きができるよう、新しいServiceのIP を記録してください。**
```
student@setX-master:~$ kubectl create deployment nginx --image=nginx

deployment.apps/nginx created
```
```
student@setX-master:~$ kubectl expose deployment nginx --type=ClusterIP --port=80

service/nginx expose
```
```
student@setX-master:~$ kubectl get svc

NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP   3d15h
nginx        ClusterIP   10.104.248.141 <none>        80/TCP    7s
```

**11. ubuntu コンテナにログインし、IP 名前解決をした後、URL の書き換え動作をテストします。**
```
student@setX-master:~$ kubectl exec -it ubuntu -- /bin/bash
```

**コンテナ内**

**(a) dig コマンドを使います。Service の名前がFQDN の一部になることに注意してください。**
```
root@ubuntu:/# dig -x 10.104.248.141

....
;; QUESTION SECTION:
;141.248.104.10.in-addr.arpa. IN PTR

;; ANSWER SECTION:
141.248.104.10.in-addr.arpa. 30 IN PTR nginx.default.svc.cluster.local.
....
```

**(b) 逆引きができたので、正引きをテストします。IP は前の手順で使ったものと同じであるべきです。**
```
root@ubuntu:/# dig nginx.default.svc.cluster.local.

....
;; QUESTION SECTION:
;nginx.default.svc.cluster.local. IN A

;; ANSWER SECTION:
nginx.default.svc.cluster.local. 30 IN A 10.104.248.141
....
```

**(c) test.io ドメインに関して追加したrewrite ルールによって、IP が解決可能かをテストします。レスポンスには、リクエスト先として指定されたFQDN ではなく、もとのService 名が含まれることに注意してください。**
```
root@ubuntu:/# dig nginx.test.io

....
;; QUESTION SECTION:
;nginx.test.io. IN A

;; ANSWER SECTION:
nginx.default.svc.cluster.local. 30 IN A 10.104.248.141
....
```

**12. コンテナから抜けてConfigMap を編集し、answer セクションを追加します。**
```
root@ubuntu:/# exit
```

**コンテナ内ここまで**

```
student@setX-master:~$ kubectl -n kube-system edit configmaps coredns

....
data:
  Corefile: |
    .:53 {
        rewrite stop {                                            #<-- この行を編集し、続く2 行を追加
           name regex (.*)\.test\.io {1}.default.svc.cluster.local
           answer name (.*)\.default\.svc\.cluster\.local {1}.test.io
          }
        errors
        health {
....
```

**13. coredns Pod を削除し、更新したConfigMap が確実に読み込まれるようにします。**
```
student@setX-master:~$ kubectl -n kube-system delete pod coredns-f9fd979d6-fv9qn coredns-f9fd979d6-lnxn5

pod "coredns-f9fd979d6-fv9qn" deleted  
pod "coredns-f9fd979d6-lnxn5" deleted 
```

**14. ubuntu コンテナに再度ログインします。今回は、レスポンスにはリクエスト先として指定されたFQDN が表示されるはずです。**
```
student@setX-master:~$ kubectl exec -it ubuntu -- /bin/bash
```

**コンテナ内**
```
root@ubuntu:/# dig nginx.test.io

....
;; QUESTION SECTION:
;nginx.test.io. IN A

;; ANSWER SECTION:
nginx.test.io. 30 IN A 10.104.248.141
....
```

**15. コンテナから抜け、DNS テスト用のツールを含んだコンテナを削除し、リソースを復元します。**
```
root@ubuntu:/# exit

student@setX-master:~$ kubectl delete -f nettool.yaml
```

以上

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter9/Lab9-4.md)
