# 課題9.4: Label を利用したリソース管理

**1. system=secondary のLabel が設定されたPod を、すべてのnamespace から削除しましょう。**
```
student@setX-master:~$ kubectl delete pods -l system=secondary --all-namespaces

pod "nginx-one-74dd9d578d-fcpmv" deleted  
pod "nginx-one-74dd9d578d-sts5l" deleted 
```

**2. Pod を確認します。Pod を管理するDeployment コントローラがまだ起動しているため、新しいPod がすぐに起動しているはずです。**
```
student@setX-master:~$ kubectl -n accounting get pods

NAME                        READY   STATUS    RESTARTS   AGE
nginx-one-74dd9d578d-ddt5r  1/1     Running   0          1m  
nginx-one-74dd9d578d-hfzml  1/1     Running   0          1m  
```

**3. Deployment にLabel も付けたので、accounting namespace でDeployment を確認してみましょう。**
```
student@setX-master:~$ kubectl -n accounting get deploy --show-labels

NAME        READY   UP-TO-DATE   AVAILABLE   AGE    LABELS
nginx-one   2/2     2            2           10m   system=secondary
```

**4. Label を指定してDeployment を削除しましょう。**
```
student@setX-master:~$ kubectl -n accounting delete deploy -l system=secondary

deployment.apps "nginx-one" deleted
```

**5. セカンダリノードからLabel を削除しましょう。ここでの書式は、削除したいキー（この場合はsystem）の直後にマイナス記号を付けます。**
```
student@setX-master:~$ kubectl label node setX-worker system-

node/setX-worker labeled
```


以上
