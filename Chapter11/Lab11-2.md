# 課題11.2: Ingress Controller

前の章で学んだ Helm を使って、Ingress Controller をインストールします。

**1. nginx を稼働させるweb-one とweb-two の 2 つの Deployment を作成します。両方共 ClusterIP Service として公開します。慣れていない場合は、前の章の内容を見て手順を確認して下さい。両方の ClusterIP が動作することを確認してから次のステップに進んで下さい。**

[参考](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter11/Lab11-3.md)


**2. Linkerd は Ingress Controller 機能を備えていないため、トラフィックを管理するために別途 Ingress Controller を追加する必要があります。Helm Chart を活用して、Ingress Controller をインストールします。Hub を検索すると、利用可能な Ingress Controller が数多く存在することがわかります。**
```
student@setX-master:~$ helm search hub ingress

URL                                                 CHART VERSION  APP VERSION DESCRIPTION
https://artifacthub.io/packages/helm/k8s-as-hel...  1.0.2          v1.0.0      Helm Chart representing a single Ingress Kubern...
https://artifacthub.io/packages/helm/openstack-...  0.2.1          v0.32.0     OpenStack-Helm Ingress Controller
...
https://artifacthub.io/packages/helm/api/ingres...  3.29.1         0.45.0      Ingress controller for Kubernetes using NGINX a...
https://artifacthub.io/packages/helm/wener/ingr...  3.31.0         0.46.0      Ingress controller for Kubernetes using NGINX a...
https://artifacthub.io/packages/helm/nginx/ngin...  0.9.2          1.11.2      NGINX Ingress Controller
< 省略>
```

**3. NGINX から提供されている人気のある Ingress Controller を利用します。**
```
student@setX-master:~$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

"ingress-nginx" has been added to your repositories
```
```
student@setX-master:~$ helm repo update

Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
Update Complete. -Happy Helming!-
```

**4. values.yamlをダウンロードして編集し、Deployment の代わりにDaemonset を利用するように変更します。これにより、全てのノードにトラフィックを制御する Pod が起動します。**
```
student@setX-master:~$ helm fetch ingress-nginx/ingress-nginx --untar
student@setX-master:~$ cd ingress-nginx
student@setX-master:~/ingress-nginx$ ls

CHANGELOG.md Chart.yaml OWNERS README.md ci templates values.yaml
```
```
変更するのは    169行目付近です。  
DeploymentをDaemonSetへ変更してください。  

student@setX-master:~/ingress-nginx$ vim values.yaml
```

![Chap11-6](/uploads/ff02dfe7a4aee01e33dff9cb48f482e2/Chap11-6.png)


**5. それでは Chart を利用して Ingress Controller をインストールします。カレントディレクトリ内を検索するために、ドット (.) を利用していることに注意して下さい。**
```
student@setX-master:~/ingress-nginx$ helm install myingress .

NAME: myingress
LAST DEPLOYED: Wed May 19 22:24:27 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running
'kubectl --namespace default get services -o wide -w myingress-ingress-nginx-controller'

An example Ingress that makes use of the controller:
< 省略>
```

**6. Ingress Controller が起動しましたが、ルールはまだ何も設定されていません。既存のリソースを見てみましょう。-w オプションを使うことで、Ingress Controller サービスが表示されるのが確認できます。Ingress Controller サービスが確認できたら、ctrl-cを使ってコマンドを終了し、次のコマンドに移ります。**
```
student@setX-master:~$ kubectl get ingress --all-namespaces

No resources found
```
```
student@setX-master:~$ kubectl --namespace default get services -o wide -w myingress-ingress-nginx-controller

NAME                               TYPE         CLUSTER-IP    EXTERNAL-IP PORT(S)                    AGE SELECTOR
myingress-ingress-nginx-controller LoadBalancer 10.104.227.79 <pending>   80:32558/TCP,443:30219/TCP 47s app.kubernetes.io/component=controller,app.kubernetes.io/instance=myingress,app.kubernetes.io/name=ingress-nginx
```
```
student@setX-master:~$ kubectl get pod --all-namespaces |grep nginx

default myingress-ingress-nginx-controller-mrqt5 1/1 Running 0 20s
default myingress-ingress-nginx-controller-pkdxm 1/1 Running 0 62s
default nginx-b68dd9f75-h6ww7                    1/1 Running 0 21h
```

**7. HTTP ヘッダーをサービスに対応させるルールを追加しましょう。**

注意：テキストのingress.yamlはこの後の演習で問題が発生する事があります。Link先のファイルを使用して下さい。  

```
student@setX-master:~$ vim ingress.yaml

```
![Chap11-12](/uploads/5daa786d09f6e487680590a7ce7b2424/Chap11-12.png)

[ingress.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter11/labfiles/ingress.yaml)


**8. Ingress ルールを作成し、動作を検証します。条件に一致するヘッダを指定しない場合、404 エラーが返却されるはずです。**
```
student@setX-master:~$ kubectl create -f ingress.yaml

ingress.networking.k8s.io/ingress-test created
```
```
student@setX-master:~$ kubectl get ingress

NAME         CLASS  HOSTS            ADDRESS PORTS AGE
ingress-test <none> www.external.com         80    5s
```
```
student@setX-master:~$ kubectl get pod -o wide |grep myingress

1 myingress-ingress-nginx-controller-mrqt5 1/1 Running 0 8m9s 192.168.219.118 2 cp <none> <none>
3 myingress-ingress-nginx-controller-pkdxm 1/1 Running 0 8m9s 192.168.219.118 4 cp <none> <none>
```
```
student@setX-master:~/ingress-nginx$ curl 192.168.219.118

<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

**9. Ingress サービスを確認し、再度404 エラーが返ることを確かめます。Admission Controller はここでは使いません。**
```
student@setX-master:~/ingress-nginx$ kubectl get svc |grep ingress

1 myingress-ingress-nginx-controller           LoadBalancer 10.104.227.79 <pending> 2 80:32558/TCP,443:30219/TCP 10m
3 myingress-ingress-nginx-controller-admission ClusterIP    10.97.132.127 <none>    4 443/TCP                    10m
```
```
student@setX-master:~/ingress-nginx$ curl 10.104.227.79

<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

**10. ここでヘッダを指定し、URL が先の手順で公開したサービスの一つに一致するようにします。nginx ウェブサーバのデフォルトページが表示されるはずです。**
```
student@setX-master:~/ingress-nginx$ curl -H "Host: www.external.com" http://10.104.227.79

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
< 省略>
```

**11. Linkerd 用の Ingress Pod に対して Annotation を追加することも可能です。いくつか警告が表示されるかもしれませんが、コマンドは動作します。**
```
student@setX-master:~/ingress-nginx$ kubectl get ds myingress-ingress-nginx-controller -o yaml | linkerd inject --ingress - | kubectl apply -f -

daemonset "myingress-ingress-nginx-controller" injected

Warning: resource daemonsets/myingress-ingress-nginx-controller is missing the　kubectl.kubernetes.io/last-applied-configuration annotation which is required
by kubectl apply. kubectl apply should only be used on resources created　declaratively by either kubectl create --save-config or kubectl apply. The　missing annotation will be patched automatically.
daemonset.apps/myingress-ingress-nginx-controller configured
```

**12. Top ペ ー ジ に 遷 移 し、 ネ ー ム ス ペ ー ス を デ フ ォ ル ト、 リ ソ ー ス を daemonset/myingress-ingress-nginx-controller に変更します。スタートを押して Ingress Controller に対してより多くのトラフィックを送り、トラフィックのメトリクスを GUI から確認します。後の手順で追加するページが見られるよう、Top ページはそのままにします。**

![Chap11-8](/uploads/c524402d337b57fb8e40db40c27cfaac/Chap11-8.png)


**13. ここからサーバをどんどん追加していきます。ここではまず 1 台のサーバを追加しますが、好きなだけ繰り返しサーバを追加することができます。**

web-two の Welcome ページをカスタマイズします。web-two Pod 内で bash シェルを起動します。vim 等のエディタをコンテナ内にインストールし、nginx のintex.htmlファイルを編集して Web ページのタイトルをInternal Welcome Page にします。下記の例ではコマンド出力の大半が省略されています。  
```
student@setX-master:~$ kubectl exec -it web-two-<Tab> -- /bin/bash
```

**コンテナ内**

```
root@web-two-...-:/# apt-get update
root@web-two-...-:/# apt-get install vim -y
root@web-two-...-:/# vim /usr/share/nginx/html/index.html

<!DOCTYPE html>
<html>
<head>
<title>Internal Welcome Page</title>   #<-- この行を編集
<style>
<省略>
```
```
root@thirdpage-:/$ exit
```

**コンテナ内ここまで**

**14. ngress ルールを編集し、thirdpage Service を指すようにします。既存のhost から始まる部分をコピーして、host とname を編集するのが最も簡単かもしれません。**
```
student@setX-master:~$ kubectl edit ingress ingress-test
```
![Chap11-9](/uploads/e75e63fd7887d1a965d6e75cb63127a0/Chap11-9.png)

[ingress-test.yaml](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter11/labfiles/ingress-test.yaml)


**15. 2 つ目のHost 設定を curl を使ってテストします。curl はローカルおよびリモートのシステムから実行します。\<title\> がデフォルトページではない内容が表示されることを確かめましょう。いずれかのノードのメイン IP を利用します。Lonkerd の GUI では新しいTO 行が表示されるはずです。矢印のついた小さな青色のボックスを選択すると、internal.org に向かうトラフィックが見られます。**
```
student@setX-master:~$ curl -H "Host: internal.org" http://10.128.0.7/

<!DOCTYPE html>
<html>
<head>
<title>Internal Welcome Page</title>
<style>
< 省略>
```

![Chap11-10](/uploads/f3a95b46df96b7f95c8604767cc38d87/Chap11-10.png)



以上

