# 課題11.1: サービスメッシュ

大量の Service をクラスタ外に公開したい場合、またはホストノードで小さい番号のポートを公開したい場合は、IngressController をデプロイしましょう。Kubernetes.io では nginx や GCE のコントローラが数多く触れられていますが、他にも多くの選択肢があります。Istio、Linkerd、Contour、Aspen 等のサービスメッシュを使うことで、より多くの機能やメトリクスを利用することができます。  

**0. ホームディレクトリに戻っておきます。**  
 
```
student@setX-master:~$ cd ~

```

**1. linkerd を、独自のスクリプトを利用してインストールします。出力が非常に多いため、全ては記載せず、一部を省略します。
出力に目を通し、すべて緑色のチェックマークが表示されることを確認して下さい。いくつかのステップは完了するのに数分かかる場合があります。記載した各コマンドにより、簡単にインストールできます。これらの手順はsetupLinkerd.txtファイルにも記載されています。**

```
student@setX-master:~$ curl -sL run.linkerd.io/install | sh
student@setX-master:~$ export PATH=$PATH:/home/student/.linkerd2/bin
student@setX-master:~$ echo "export PATH=$PATH:/home/student/.linkerd2/bin" >> $HOME/.bashrc
student@setX-master:~$ linkerd check --pre
student@setX-master:~$ linkerd install --crds | kubectl apply -f -
student@setX-master:~$ linkerd install | kubectl apply -f -
student@setX-master:~$ linkerd check
student@setX-master:~$ linkerd viz install | kubectl apply -f -
student@setX-master:~$ linkerd viz check
student@setX-master:~$ linkerd viz dashboard &

# linkerd check, linkerd viz checkで数分時間がかかるかもしれません。

# $ linkerd viz checkで次のエラーが出た場合は1分後再実行してみてください。
# × can initialize the client
#    no running pods found for metrics-api
#    see https://linkerd.io/2.13/checks/#l5d-viz-existence-client for hints
#
#Status check results are ×

```

**2. デフォルトでは、GUI は localhost に提供されています。ノードに Cloud Provider を利用している場合は、Service とDeployment
を編集して外部からのアクセスを許可する必要があります。59 行付近にある、-enforced-host のイコールより後の文字列を全て削除します。**
```
student@setX-master:~$ kubectl -n linkerd-viz edit deploy web
```
![Chap11-1](/uploads/de03e6e5f7a6c0fc1bbe798c85a0f3b7/Chap11-1.png)


**3. http nodePort を編集し、type を NodePort に変更。**
```
student@setX-master:~$ kubectl edit svc web -n linkerd-viz

nodePort: 31500 を追加する行は33行目付近
type: NodePort を編集する行は45行目付近

```
![Chap11-2](/uploads/04cac57819f5423f84fab7e4b8f4fedc/Chap11-2.png)


**4. ローカルのブラウザを使ってパブリック IP へのアクセスをテストします。IP は下記のものと異なるでしょう。**
```
student@setX-master:~$ curl ifconfig.io

104.197.159.20
```

**5. ローカルのシステムから、ブラウザを開き、パブリック IP およびポート番号が大きな nodePort にアクセスします。ソフトウェアが
定期的に更新されるため、ウェブページの見た目が多少違うかもしれません。例えば、Grafana はもはや完全には統合されていません。**

![Chap11-3](/uploads/5e02dca96c265fbbdc6cf4afbf1c479e/Chap11-3.png)


**6. linkerd がオブジェクトを監視するようためには、アノテーションを追加する必要があります。linkerd inject コマンドを利用して設定できます。YAML ファイルを生成して linkerd コマンドに pipe を使って入力し、その出力をさらに kubectl コマンドに pipe で入力します。オブジェクト作成に関するエラーが表示されるかもしれませんが、処理は動作するはずです。バックスラッシュを省略することでコマンドを 1 行で実行できます。前の演習で利用したnginx-one Deployment を再作成します。**
```
nginx-one Deployment再作成：
・accounting namespaceは残っているはずです。なければ作成します。  
・各ノードに system=secondOne のラベルを割り当てます。  
・nginx-one.yaml を使ってデプロイします。

student@setX-master:~$ kubectl label node setXX-master system=secondOne
student@setX-master:~$ kubectl label node setXX-worker system=secondOne  #ワーカーノードにも割り当てます
student@setX-master:~$ kubectl apply -f nginx-one.yaml
student@setX-master:~$ kubectl get deployments.apps  -n accounting

```
その後アノテーションを追加  

```
student@setX-master:~$ kubectl -n accounting get deploy nginx-one -o yaml | linkerd inject - | kubectl apply -f -

deployment "nginx-one" injected
deployment.apps/nginx-one configured
```

**7. GUI を見ると、accounting ネームスペースと Pod が繋がれ、名前がリンクになっているはずです。**

![Chap11-11](/uploads/6bff319f325751b0eb45902a05ce870c/Chap11-11.png)

**8. Pod に対するトラフィックを生成し、トラフィックを GUI で観察します。service-lab Service を使います。**
```
student@setX-master:~$ kubectl -n accounting get svc

NAME        TYPE      CLUSTER-IP     EXTERNAL-IP PORT(S)      AGE
nginx-one   ClusterIP 10.107.141.227 <none>      8080/TCP     5h15m
service-lab NodePort  10.102.8.205   <none>      80:30759/TCP 5h14m
```
```
student@setX-master:~$ curl 10.102.8.205

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
< 省略>
```

![Chap11-4](/uploads/c92a2c3c9b9c9775f25ab3d53df12588/Chap11-4.png)


**9.  nginx-one Deployment をスケールアップします。トラフィックを生成し、各 Pod のメトリクスを取得します。**
```
student@setX-master:~$ kubectl -n accounting scale deploy nginx-one --replicas=5

deployment.apps/nginx-one scaled
```
```
student@setX-master:~$ curl 10.102.8.205     #何回か実行してみましょう
```

**10. GUI が提供するその他の情報を見てみましょう。最初にdefault ネームスペースが表示されることに注意して下さい。accounting ネームスペースに切り替え、nginx-one Deployment の詳細を確認します。**

![Chap11-5](/uploads/be5e56ad181cc1ed2500f1ab24690cfd/Chap11-5.png)


以上  

[Next](https://gitlab.com/hpe-education-japan/kubernetes-administration-1.26/-/blob/main/Chapter11/Lab11-2.md)

